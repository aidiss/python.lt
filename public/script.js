function getItems() {
  if (typeof items !== "undefined") {
    items.forEach(function (item) {
      var node = document.createElement("li");
      var link = document.createElement("a");
      link.href = item.link;
      var textnode = document.createTextNode(item.name);
      link.appendChild(textnode);
      node.appendChild(link);
      document.getElementById("links").appendChild(node);
    });
  }
}

getItems();
