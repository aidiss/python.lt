const items = [
  { name: "PyCon Lithuania - kasmetinė tarptautinė konferencija", link: "https://pycon.lt/" },
  { name: "Python Lietuva facebook grupė", link: "https://www.facebook.com/groups/pythonlt/" },
  { name: "Angis", link: "https://angis.net/#/" },
  { name: "Angis github", link: "https://github.com/mantasurbonas/angis" },
  { name: "Python Vilnius", link: "https://www.meetup.com/Python-Vilnius/" },
  {
    name: "Lietuvių kalbos rašybos tikrintuvai bei Hunspell žodynai gramatika",
    link: "https://github.com/Semantika2/Lietuviu-kalbos-rasybos-tikrintuvai-bei-Hunspell-zodynai-gramatika",
  },
  { name: "Spacy Lietuviu kalbai", link: "https://spacy.io/models/lt" },
  { name: "IT Brandos egzaminų sprendimai ", link: "https://github.com/python-dirbtuves/it-brandos-egzaminai" },

  { name: "Github projektai su žyme Lietuva", link: "https://github.com/topics/lithuanian" },
  // Meetup
  { name: "PyData Vilnius", link: "https://www.meetup.com/PyData-Vilnius/" },
  { name: "PyData Kaunas", link: "https://www.meetup.com/PyData-Kaunas/" },
  { name: "VilniusPy", link: "https://www.meetup.com/VilniusPy" },
  { name: "KaunasPy", link: "https://www.meetup.com/KaunasPy" },

  //   Youtube
  { name: "Programavimas Python", link: "https://www.youtube.com/watch?v=IuByH_vrwGA&list=PLB7EF2523A58A7854" }, // Youtube Playlist
  {
    name: "Griaustinis tech Django",
    link: "https://www.youtube.com/watch?v=998PannJtHo&list=PL3aaklOBGuJmxk9mrsbbWd45WYFyOWfLc&ab_channel=GriaustinisTech", // Youtube Playlist
  },
  // News
  {
    name: "NSA informacinių technologijų egzamine nauja programavimo kalba",
    link: "https://www.nsa.smm.lt/2022/06/13/informaciniu-technologiju-egzamine-nauja-programavimo-kalba/",
  },
  {
    name: "Informatikos mokytojai dalyvavo seminaruose apie Pitono programavimo kalbos mokymo patirti",
    link: "https://if.ktu.edu/news/informatikos-mokytojai-dalyvavo-seminaruose-apie-pitono-programavimo-kalbos-mokymo-patirti/",
  },
];
