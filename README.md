# python.lt

## Pakeitimų darymas
python.lt puslapyje yra rodomos šios repozitorijos `public` direktorijos turinys.
Norint padaryti pakeitimų python.lt svetainėje užtenka sukurti Pull Requestą, kuris
redaguoja `public` direktorijos turinį - patvirtinus ir sumerginus Pull Requestą
į `main` branch bus įvykdytas CI/CD pipeline, kuris įdiegs pakeitimus ir po minutės
jie matysis python.lt svetainėje.

## Vizija
Aidžio brainstormo mintys:
- Nuorodos į konferencijas ir meetupus.
- Nuoroda į Python Lietuva facebook grupę
- Nuoroda į Angis
- Nuorodos į mokymosi šaltinius, prioritetas turėtų būti skiriamas lietuviškiems arba lietuvių kurtiems. Taip pat aukštos kokybės atrinkti šaltiniais anglų kalba.
- Nuoroda į twitter hashtagus ir userius
- Nuorodos į youtube kūrėjus. Pvz Griaustinis Tech
- Lietuvos kompanijų kuriose naudojamas Python aprašymas, įskaitant tai, kaip jose naudojamas Python.
- Studijų programos, mokymai. Dėl šito dvejoju, nes čia for profit dalykas. Bet gal su atitinkamu kritišku aprašymu ir perspėjimais aklai nepasitikėti, gal ir paeitų.

Apibendrinant įsivaizduoju kad python.lt turėtų būti trys ramsčiai
- Python
- Lietuvoje (Lietuviškos kompanijos, renginiai ir t.t.)
- Lietuviškai (Bandom palaikyti lietuvių kalbą)

Bei papildant labai aukštos kokybės informacija angliškai.
